"use strict";

describe( "Test de formulario de ingreso", function() {
  beforeEach( function() {
    browser.ignoreSynchronization = false;
    browser.get( "/" );
    browser.sleep( 2000 );
  } );

  it( "Deslogueo del sistema", pruebaDeslogueo );

  function pruebaLogin() {
    var usuario = element( by.model( "vm.usuario.login" ) );
    var contrasena = element( by.model( "vm.usuario.password" ) );
    usuario.sendKeys( "admin" );
    contrasena.sendKeys( "odooCiris.93" );
    element( by.css( ".form-horizontal button[type='submit']" ) ).click();
    browser.driver.wait( function( ) {
      return browser.driver.getCurrentUrl( ).then( function( url ) {

        //browser.pause();
        return ( /principal/ ).test( url );
      } );
    } );
  }

  function pruebaDeslogueo() {
    pruebaLogin();
    element( by.css( ".dropdown-toggle" ) ).click();
    element( by.css( "[ng-click='nav.salir()']" ) ).click();
    expect( true ).toEqual( true );
  }

} );
