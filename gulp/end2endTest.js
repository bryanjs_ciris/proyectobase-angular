"use strict";

exports.tests = tests;
var gulp = require( "gulp" );
var protractor = require( "gulp-protractor" ).protractor;
var spawn = require( "child_process" );
var path = require( "path" );

function tests() {
  var comando = path.resolve( "./node_modules/protractor/bin/webdriver-manager" ) + " update";
  console.log( comando );
  var actualizarWebDrive = spawn.exec( comando,
    function( error, stdout ) {
      if ( error ) {
        console.log( error.stack );
        console.log( "Codigo del error: " + error.code );
      }
      console.log( stdout );
    } );

  actualizarWebDrive.on( "exit", function( ) {
    var args = [ "--baseUrl", "http://localhost:3000/" ];
    gulp.src( [ "../e2e/app/**/vistas/*.js" ] )
        .pipe( protractor( {
          configFile: "./protractor.conf.js",
          args: args
        } ) )
        .on( "error", function( e ) {
          console.log( e );
        } );
  } );
}
