"use strict";

module.exports = FormNOMBRECtrl;

FormNOMBRECtrl.$inject = [ "ARCHIVO", "NOMBREAPI", "$state", "$stateParams", "$auth",
"Validaciones" ];
function FormNOMBRECtrl( ARCHIVO, NOMBREAPI, $state, $stateParams, $auth, Validaciones ) {
  var vm = this;
  var permisos = $auth.getPayload().permisos.ARCHIVO;
  vm.ARCHIVO = ARCHIVO;
  vm.ARCHIVO.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;

  if ( vm.ARCHIVO._id ) {
    if ( vm.ARCHIVO.editando ) {
      Validaciones.validar( permisos, "editar" );
    } else {
      Validaciones.validar( permisos, "ver" );
    }
  } else {
    Validaciones.validar( permisos, "crear" );
  }

  function guardar( formValido ) {
    if ( formValido ) {
      NOMBREAPI.guardar( vm.ARCHIVO ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function editar( valor ) {
    reload( vm.ARCHIVO._id, valor );
  }

  function eliminar() {
    NOMBREAPI.eliminar( vm.ARCHIVO ).then( function() {
      $state.go( "^.ARCHIVO-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }
}
