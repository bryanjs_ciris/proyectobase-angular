"use strict";

var gulp = require( "gulp" );
var jsinspect = require( "gulp-jsinspect" );
var rutas = require( "../rutas" );
var gutil = require( "gulp-util" );
var plumber = require( "gulp-plumber" );
var notify = require( "gulp-notify" );

module.exports = inspeccionar;
var mensaje = "Se ha encontrado código repetido (Ctrl + C → Ctrl + V)." +
  " Refactorice en una función común!";

function inspeccionar() {
  return gulp.src( rutas.scripts.watch )
    .pipe( plumber( {
      errorHandler: notify.onError( mensaje )
    } ) )
    .pipe( jsinspect( {
      "threshold": 30,
      "identifiers": true,
      "suppress": 100
    } ) )
    .on( "error", function( e ) {
      gutil.log( e );
      this.emit( "end" );
    } );
}
