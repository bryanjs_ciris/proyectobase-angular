"use strict";

var gulp = require( "gulp" );
var complexity = require( "gulp-complexity" );
var rutas = require( "../rutas" );
var gutil = require( "gulp-util" );
var plumber = require( "gulp-plumber" );
var notify = require( "gulp-notify" );

module.exports = complejidad;

function complejidad() {
  return gulp.src( rutas.scripts.watch )
    .pipe( plumber( {
      errorHandler: notify.onError( "Error: <%= error.message %>" )
    } ) )
    .pipe( complexity( {
      "breakOnErrors": false
    } ) )
    .on( "error", gutil.log );
}
