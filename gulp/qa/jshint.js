"use strict";

var gulp = require( "gulp" );
var jshint = require( "gulp-jshint" );
var rutas = require( "../rutas" );
var gutil = require( "gulp-util" );
var plumber = require( "gulp-plumber" );
var notify = require( "gulp-notify" );

module.exports = jsHint;

function jsHint() {
  return gulp.src( rutas.scripts.watch )
    .pipe( plumber( {
      errorHandler: notify.onError( "Error: <%= error.message %>" )
    } ) )
    .pipe( jshint() )
    .pipe( notify( function( file ) {
      if ( file.jshint.success ) {
        return false;
      }
      var errors = file.jshint.results.map( function( data ) {
        if ( data.error ) {
          return "(" + data.error.line + ":" + data.error.character + ") " + data.error.reason;
        }
      } ).join( "\n" );
      return file.relative + " (" + file.jshint.results.length + " errores)\n" + errors;
    } ) )
    .pipe( jshint.reporter( "jshint-stylish" ) )
    .on( "error", gutil.log );
}
