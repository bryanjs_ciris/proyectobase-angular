"use strict";

var gulp = require( "gulp" );
var jscs = require( "gulp-jscs" );
var rutas = require( "../rutas" );
var gutil = require( "gulp-util" );
var plumber = require( "gulp-plumber" );
var notify = require( "gulp-notify" );

exports.src = src;
exports.test = test;

function src() {
  return gulp.src( rutas.scripts.watch )
    .pipe( plumber( {
      errorHandler: notify.onError( "Error: <%= error.message %>" )
    } ) )
    .pipe( jscs( {fix: true} ) )
    .pipe( jscs.reporter() )
    .pipe( gulp.dest( rutas.scripts.base ) )
    .on( "error", gutil.log );
}

function test() {
  return gulp.src( rutas.scripts.tests.watch )
    .pipe( plumber( {
      errorHandler: notify.onError( "Error: <%= error.message %>" )
    } ) )
    .pipe( jscs( {fix: true} ) )
    .pipe( jscs.reporter() )
    .pipe( gulp.dest( rutas.scripts.tests.base ) )
    .on( "error", gutil.log );
}
