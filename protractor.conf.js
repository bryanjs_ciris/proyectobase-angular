var versions = require( "./node_modules/protractor/config.json" ).webdriverVersions;
exports.config = {

  //seleniumAddress: "http://localhost:4444/wd/hub",//Se comenta xq se agrega el jar de selenium
  seleniumServerJar: "./node_modules/protractor/selenium/selenium-server-standalone-" +
  versions.selenium + ".jar",
  seleniumPort: 4444,
  specs: [ "./e2e/app/**/vistas/*.js" ],
  multiCapabilities: [ {
    browserName: "phantomjs"
  } ],
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    isVerbose: true,
    includeStackTrace: true
  }

  //directConnect: true
};
