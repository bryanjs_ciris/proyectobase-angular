"use strict";

module.exports = Usuario;

function Usuario( nombre, foto, sesionOdoo, token, _id, correo, estado, rol, permisos ) {
  this.nombre = nombre || null;
  this.foto = foto || null;
  this.sesionOdoo = sesionOdoo || null;
  this.token = token || null;
  this._id = _id;
  this.correo = correo || null;
  this.estado = estado || null;
  this.rol = rol || null;
  this.permisos = permisos || {};
  this.elementosPorPagina = 10;
}

Usuario.cargar = cargar;

/*******************************************************  FUNCIONES PÚBLICAS  */

function cargar( json ) {
  if ( json ) {
    if ( _.isArray( json ) ) {
      return _.map( json, function( elem ) {
        return instanciar( elem );
      } );
    } else {
      return instanciar( json );
    }
  }
}

function instanciar( json ) {
  return new Usuario( json.nombre, json.foto, json.sesionOdoo, json.token, json._id, json.correo,
    json.estado, json.rol, json.permisos );
}
