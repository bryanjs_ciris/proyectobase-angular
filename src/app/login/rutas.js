"use strict";

module.exports = rutas;
rutas.$inject = [ "$stateProvider" ];

function rutas( $stateProvider ) {

  $stateProvider.state( "login", {
    url: "/",
    templateUrl: "login/vistas/login.html",
    controller: "LoginCtrl",
    controllerAs: "vm",
    libre:  true
  } );

} //rutas
