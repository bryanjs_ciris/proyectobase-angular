"use strict";

var modulo = angular.module( "ProyectoBase.login", [] );

modulo.config( require( "./rutas" ) );

modulo.controller( "LoginCtrl", require( "./controladores/login.js" ) );

modulo.factory( "UsuarioAPI", require( "./controladores/rest/usuarioAPI.js" ) );

module.exports = modulo;
