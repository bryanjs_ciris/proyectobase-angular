"use strict";

module.exports = UsuarioAPI;
var Usuario = require( "../../modelos/usuario.js" );

UsuarioAPI.$inject = [ "$http", "urlApi", "Notificaciones" ];
function UsuarioAPI( $http, urlApi, Notificaciones ) {
  var url = urlApi + "/api/usuario";
  UsuarioAPI.yo = yo;
  UsuarioAPI.buscar = buscar;
  UsuarioAPI.obtener = obtener;
  UsuarioAPI.listar = listar;
  UsuarioAPI.guardar = guardar;
  return UsuarioAPI;

  function yo() {
    return $http.get( url + "/yo" ).then( Usuario.cargar, alerta );
  }

  function buscar( texto ) {
    var params = {
      params: {
        texto: texto,
        pagina: 0,
        cantidad: 10
      }
    };
    return $http.get( url + "/busqueda/", params ).then( Usuario.cargar, alerta );
  }

  function alerta( resp ) {
    console.error( resp );
    Notificaciones.agregar( resp.status, "Usuario" );
    return resp.data;
  }

  function obtener( id ) {
    if ( id ) {
      return $http.get( urlApi + "/api/usuario/" + id ).then( function( resp ) {
        return Usuario.cargar( resp.data );
      } );
    } else {
      return new Usuario();
    }
  }

  function listar( pagina, cantidad ) {
    var params = {
      params: {
        pagina: pagina || 0,
        cantidad: cantidad || 10
      }
    };
    return $http.get( urlApi + "/api/usuario/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = Usuario.cargar( resp.data.docs );
    }
    return resp.data;
  }

  function err( resp ) {
    Notificaciones.agregar( resp.status, "Usuario" );
    return [];
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( urlApi + "/api/usuario/", obj ).then( function( resp ) {
      Notificaciones.agregar( resp.status, "Usuario" );
      return Usuario.cargar( resp.data );
    }, function( resp ) {
      Notificaciones.agregar( resp.status, "Usuario" );
      return resp;
    } );
  }

  function editar( obj ) {
    return $http.put( urlApi + "/api/usuario/" + obj._id, obj ).then( function( resp ) {
      Notificaciones.agregar( resp.status, "Usuario" );
      return Usuario.cargar( resp.data );
    }, function( resp ) {
      Notificaciones.agregar( resp.status, "Usuario" );
      return resp;
    } );
  }

}
