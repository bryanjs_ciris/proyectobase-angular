"use strict";

module.exports = LoginCtrl;
var Usuario = require( "../modelos/usuario.js" );

LoginCtrl.$inject = [ "$auth", "Notificaciones", "$state", "$sessionStorage", "$localStorage" ];
function LoginCtrl( $auth, Notificaciones, $state, $sessionStorage, $localStorage ) {
  var vm = this;
  vm.ingresar = ingresar;
  vm.usuario = {
    recordar: true
  };

  function ingresar( form, usuario ) {
    if ( form.$valid ) {
      $auth.login( usuario ).then( ok, alerta );
    } else {
      return Notificaciones.agregarPersonalizado( 400, "Faltan campos por completar" );
    }
  }

  function ok( res ) {
    var usuario = Usuario.cargar( res.data.usuario );
    $sessionStorage.usuario = usuario;
    if ( vm.usuario.recordar === true ) {
      $localStorage.usuario = usuario;
    }
    Notificaciones.agregarPersonalizado( 200, "Bienvenido " + usuario.nombre );
    $state.go( "inicio.principal" );
  }

  function alerta( resp ) {
    if ( resp.status === -1 ) {
      Notificaciones.agregarPersonalizado( 500, "Sin conexión con el servidor" );
    }
    Notificaciones.agregarPersonalizado( resp.status, "Credenciales Inválidos" );
  }
}
