"use strict";

var angular = window.angular;

var modulo = angular.module( "ProyectoBase.base", [] );

modulo.config( require( "./rutas" ) );

modulo.controller( "NavegacionCtrl", require( "./servicios/navegacionCtrl.js" ) );

modulo.factory( "Notificaciones", require( "./servicios/notificaciones" ) );
modulo.factory( "Futuros", require( "./servicios/futuros" ) );
modulo.factory( "Validaciones", require( "./servicios/validaciones.js" ) );

modulo.directive( "clickEn", require( "./directivas/clickEn.js" ) );
modulo.directive( "cisSwitch", require( "./directivas/cisSwitch.js" ) );
modulo.directive( "cisTitulo", require( "./directivas/cisTitulo.js" ) );
modulo.directive( "cisMinimizar", require( "./directivas/cisMinimizar.js" ) );
modulo.directive( "numerico", require( "./directivas/cisInputNumerico.js" ) );
modulo.directive( "cisEnter", require( "./directivas/cisEnter.js" ) );
modulo.directive( "ocultarAccion", require( "./directivas/ocultarAccion.js" ) );

var filtros = require( "./filtros" );
modulo.filter( "Fecha", filtros.fecha );
modulo.filter( "Porcentaje", filtros.porcentaje );

module.exports = modulo;
