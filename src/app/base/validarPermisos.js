"use strict";

module.exports = function( ruta, permisos ) {
  if ( ruta === "inicio.administracion.rol-lista" && !permisos.rol.listar ) {
    return false;
  }
  if ( ruta === "inicio.administracion.usuario-lista" && !permisos.usuario.listar ) {
    return false;
  }
  return true;
};
