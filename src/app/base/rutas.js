"use strict";

module.exports = rutas;
rutas.$inject = [ "$stateProvider", "$urlRouterProvider" ];

function rutas( $stateProvider, $urlRouterProvider ) {

  $stateProvider.state( "inicio", {
    templateUrl: "base/vistas/plantilla.html",
    abstract: true
  } );

  $stateProvider.state( "404", {
    url: "/404",
    templateUrl: "base/vistas/404.html",
    libre: true,
    data: {
      titulo: "Página no encontrada"
    }
  } );

  $stateProvider.state( "403", {
    url: "/403",
    templateUrl: "base/vistas/403.html",
    libre: true,
    data: {
      titulo: "Acceso denegado"
    }
  } );

  $urlRouterProvider.otherwise( "/" );
} //rutas
