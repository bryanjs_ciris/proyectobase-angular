"use strict";

module.exports = Validaciones;

Validaciones.$inject = [ "$state" ];

function Validaciones( $state ) {
  Validaciones.validar = validar;
  return Validaciones;

  function validar( permisosUsuario, permisoAValidar ) {
    if ( !permisosUsuario[permisoAValidar] ) {
      return $state.go( "403" );
    }
  }
}
