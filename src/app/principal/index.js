"use strict";

var modulo = angular.module( "ProyectoBase.Principal", [] );
modulo.config( require( "./rutas" ) );

modulo.controller( "PrincipalCtrl", require( "./controladores/principalCtrl.js" ) );

module.exports = modulo;
