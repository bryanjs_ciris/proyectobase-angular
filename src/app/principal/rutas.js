"use strict";

module.exports = rutas;

rutas.$inject = [ "$stateProvider" ];
function rutas( $stateProvider ) {

  $stateProvider.state( "inicio.principal", {
    templateUrl: "principal/vistas/inicio.html",
    url: "/principal",
    controller: "PrincipalCtrl",
    controllerAs: "mod",
    data: {
      titulo: "Módulo Principal",
      icono: "fa-home",
      menu: "Principal"
    }
  } );

}
