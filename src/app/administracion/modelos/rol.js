"use strict";

module.exports = Rol;

function Rol( nombre, descripcion, permisos, _id ) {
  this.nombre = nombre || null;
  this.descripcion = descripcion || null;
  this.permisos = permisos || {};
  this._id = _id;
}

Rol.cargar = cargar;

function cargar( json ) {
  if ( json ) {
    if ( _.isArray( json ) ) {
      return _.map( json, function( elem ) {
        return instanciar( elem );
      } );
    } else {
      return instanciar( json );
    }
  }
}

function instanciar( json ) {
  return new Rol( json.nombre, json.descripcion, json.permisos, json._id );
}
