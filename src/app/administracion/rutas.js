"use strict";

module.exports = rutas;

rutas.$inject = [ "$stateProvider" ];
function rutas( $stateProvider ) {

  $stateProvider.state( "inicio.administracion", {
    templateUrl: "administracion/vistas/inicio.html",
    url: "/administracion",
    controller: "AdministracionCtrl",
    controllerAs: "mod",
    data: {
      titulo: "Módulo Administración",
      icono: "fa-cog",
      menu: "Administración"
    }
  } );

  $stateProvider.state( "inicio.administracion.usuario-lista", {
    templateUrl: "administracion/vistas/listaUsuario.html",
    url: "/usuario?pagina&cantidad",
    controller: "ListadoUsuarioCtrl",
    controllerAs: "vm",
    resolve: {
      listado: listarUsuario
    },
    data: {
      titulo: "Listado de Usuarios",
      icono: "fa-list",
      menu: "Listar Usuarios"
    }
  } );

  $stateProvider.state( "inicio.administracion.usuario-uno", {
    templateUrl: "administracion/vistas/formUsuario.html",
    url: "/usuario/:id?editar",
    controller: "FormUsuarioCtrl",
    controllerAs: "vm",
    resolve: {
      usuario: obtenerUsuario
    },
    data: {
      titulo: "Form de Usuario",
      icono: "fa-file-o",
      menu: "Form Usuario"
    }
  } );

  listarUsuario.$inject = [ "UsuarioAPI", "$stateParams" ];
  function listarUsuario( UsuarioAPI, $stateParams ) {
    return UsuarioAPI.listar( $stateParams.pagina, $stateParams.cantidad );
  }

  obtenerUsuario.$inject = [ "UsuarioAPI", "$stateParams" ];
  function obtenerUsuario( UsuarioAPI, $stateParams ) {
    return UsuarioAPI.obtener( $stateParams.id, $stateParams.editar );
  }

  $stateProvider.state( "inicio.administracion.rol-lista", {
    templateUrl: "administracion/vistas/listaRol.html",
    url: "/rol?pagina&cantidad",
    controller: "ListadoRolCtrl",
    controllerAs: "vm",
    resolve: {
      listado: listarRol
    },
    data: {
      titulo: "Listado de Roles",
      icono: "fa-list",
      menu: "Listar Rol"
    }
  } );

  $stateProvider.state( "inicio.administracion.rol-uno", {
    templateUrl: "administracion/vistas/formRol.html",
    url: "/rol/:id?editar",
    controller: "FormRolCtrl",
    controllerAs: "vm",
    resolve: {
      rol: obtenerRol
    },
    data: {
      titulo: "Form de Rol",
      icono: "fa-file-o",
      menu: "Form Rol"
    }
  } );

  listarRol.$inject = [ "RolAPI", "$stateParams" ];
  function listarRol( RolAPI, $stateParams ) {
    return RolAPI.listar( $stateParams.pagina, $stateParams.cantidad );
  }

  obtenerRol.$inject = [ "RolAPI", "$stateParams" ];
  function obtenerRol( RolAPI, $stateParams ) {
    return RolAPI.obtener( $stateParams.id, $stateParams.editar );
  }

}
