"use strict";

module.exports = RolAPI;

var Rol = require( "../../modelos/rol.js" );

RolAPI.$inject = [ "urlApi", "$http", "Notificaciones" ];

function RolAPI( urlApi, $http, Notificaciones ) {
  RolAPI.obtener = obtener;
  RolAPI.listar = listar;
  RolAPI.guardar = guardar;
  RolAPI.eliminar = eliminar;
  return RolAPI;

  function obtener( id ) {
    if ( id ) {
      return $http.get( urlApi + "/api/rol/" + id ).then( function( resp ) {
        return Rol.cargar( resp.data );
      } );
    } else {
      return new Rol();
    }
  }

  function listar( pagina, cantidad ) {
    var params = {
      params: {
        pagina: pagina || 0,
        cantidad: cantidad || 10
      }
    };
    return $http.get( urlApi + "/api/rol/", params ).then( ok, err );
  }

  function ok( resp ) {
    if ( resp.data.docs ) {
      resp.data.docs = Rol.cargar( resp.data.docs );
    }
    return resp.data;
  }

  function err( resp ) {
    Notificaciones.agregar( resp.status, "Rol" );
    return [];
  }

  function eliminar( id ) {
    return $http.delete( urlApi + "/api/rol/" + id ).then( function( resp ) {
      Notificaciones.agregar( resp.status, "Rol" );
      return resp.data;
    }, function( resp ) {
      Notificaciones.agregar( resp.status, "Rol" );
      return resp;
    } );
  }

  function guardar( obj ) {
    if ( obj._id ) {
      return editar( obj );
    } else {
      return crear( obj );
    }
  }

  function crear( obj ) {
    return $http.post( urlApi + "/api/rol/", obj ).then( function( resp ) {
      Notificaciones.agregar( resp.status, "Rol" );
      return Rol.cargar( resp.data );
    }, function( resp ) {
      Notificaciones.agregar( resp.status, "Rol" );
      return resp;
    } );
  }

  function editar( obj ) {
    return $http.put( urlApi + "/api/rol/" + obj._id, obj ).then( function( resp ) {
      Notificaciones.agregar( resp.status, "Rol" );
      return Rol.cargar( resp.data );
    }, function( resp ) {
      Notificaciones.agregar( resp.status, "Rol" );
      return resp;
    } );
  }
}
