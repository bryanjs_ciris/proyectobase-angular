"use strict";

module.exports = ListadoUsuarioCtrl;

ListadoUsuarioCtrl.$inject = [ "listado", "UsuarioAPI", "$state", "$stateParams" ];
function ListadoUsuarioCtrl( listado, UsuarioAPI, $state, $stateParams ) {
  var vm = this;
  vm.pagina = parseInt( $stateParams.pagina || 0 ) + 1;
  vm.cantidad = parseInt( $stateParams.cantidad || 10 );
  vm.listado = listado;
  vm.eliminar = eliminar;
  vm.actualizarPagina = actualizarPagina;

  function eliminar( usuario ) {
    if ( confirm( "¿Está seguro que desea eliminar el Usuario?" ) ) {
      UsuarioAPI.eliminar( usuario._id ).then( function() {
        vm.listado.docs = _.reject( vm.listado.docs, function( elem ) {
          return elem._id === usuario._id;
        } );
        vm.listado.contador -= 1;
      } );
    }
  }

  function actualizarPagina( pagina ) {
    $state.go( $state.current, {pagina: pagina, cantidad: vm.cantidad}, {reload: false} );
  }
}
