"use strict";

module.exports = FormRolCtrl;

FormRolCtrl.$inject = [ "rol", "RolAPI", "$state", "$stateParams", "$auth",
"Validaciones" ];
function FormRolCtrl( rol, RolAPI, $state, $stateParams, $auth, Validaciones ) {
  var vm = this;
  var permisos = $auth.getPayload().permisos.rol;
  vm.rol = rol;
  vm.rol.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;

  if ( vm.rol._id ) {
    if ( vm.rol.editando ) {
      Validaciones.validar( permisos, "editar" );
    } else {
      Validaciones.validar( permisos, "ver" );
    }
  } else {
    Validaciones.validar( permisos, "crear" );
  }

  function guardar( formValido ) {
    if ( formValido ) {
      RolAPI.guardar( vm.rol ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function editar( valor ) {
    reload( vm.rol._id, valor );
  }

  function eliminar() {
    RolAPI.eliminar( vm.rol ).then( function() {
      $state.go( "^.rol-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }
}
