"use strict";

module.exports = FormUsuarioCtrl;

FormUsuarioCtrl.$inject = [ "usuario", "UsuarioAPI", "RolAPI", "$state", "$stateParams", "$auth",
"Validaciones" ];
function FormUsuarioCtrl( usuario, UsuarioAPI, RolAPI, $state, $stateParams, $auth, Validaciones ) {
  var vm = this;
  var permisos = $auth.getPayload().permisos.usuario;
  vm.usuario = usuario;
  vm.idUsuarioLogueado = $auth.getPayload().sub;
  vm.usuario.editando = ( $stateParams.editar === "true" );
  vm.guardar = guardar;
  vm.editar = editar;
  vm.eliminar = eliminar;
  vm.aplicarPermisos = aplicarPermisos;
  listarRoles();

  if ( vm.usuario._id ) {
    if ( vm.usuario.editando ) {
      Validaciones.validar( permisos, "editar" );
    } else {
      Validaciones.validar( permisos, "ver" );
    }
  } else {
    Validaciones.validar( permisos, "crear" );
  }

  function guardar( formValido ) {
    if ( formValido ) {
      UsuarioAPI.guardar( vm.usuario ).then( function( resp ) {
        reload( resp._id, false );
      } );
    }
  }

  function editar( valor ) {
    reload( vm.usuario._id, valor );
  }

  function eliminar() {
    UsuarioAPI.eliminar( vm.usuario ).then( function() {
      $state.go( "^.usuario-lista" );
    } );
  }

  function reload( id, editando ) {
    $state.go( $state.current, {id: id, editar: editando}, {reload: true} );
  }

  function listarRoles() {
    RolAPI.listar().then( function( resp ) {
      vm.listaRoles = resp.docs;
    } );
  }

  function aplicarPermisos( rol ) {
    var i = _.findIndex( vm.listaRoles, function( elem ) {
      return elem.nombre === rol;
    } );
    Object.assign( vm.usuario.permisos, vm.listaRoles[i].permisos );
  }
}
