"use strict";

module.exports = ListadoRolCtrl;

ListadoRolCtrl.$inject = [ "listado", "RolAPI", "$state", "$stateParams" ];
function ListadoRolCtrl( listado, RolAPI, $state, $stateParams ) {
  var vm = this;
  vm.pagina = parseInt( $stateParams.pagina || 0 ) + 1;
  vm.cantidad = parseInt( $stateParams.cantidad || 10 );
  vm.listado = listado;
  vm.eliminar = eliminar;
  vm.actualizarPagina = actualizarPagina;

  function eliminar( rol ) {
    if ( confirm( "¿Está seguro que desea eliminar el Rol?" ) ) {
      RolAPI.eliminar( rol._id ).then( function() {
        vm.listado.docs = _.reject( vm.listado.docs, function( elem ) {
          return elem._id === rol._id;
        } );
        vm.listado.contador -= 1;
      } );
    }
  }

  function actualizarPagina( pagina ) {
    $state.go( $state.current, {pagina: pagina, cantidad: vm.cantidad}, {reload: false} );
  }
}
