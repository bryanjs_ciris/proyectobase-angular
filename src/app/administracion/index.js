"use strict";

var nombre = require( "../../../package.json" ).name;

var modulo = angular.module( nombre + ".Administracion", [] );
modulo.config( require( "./rutas" ) );

modulo.controller( "AdministracionCtrl", require( "./controladores/administracionCtrl.js" ) );

modulo.controller( "ListadoUsuarioCtrl", require( "./controladores/listadoUsuarioCtrl.js" ) );

modulo.controller( "FormUsuarioCtrl", require( "./controladores/formUsuarioCtrl.js" ) );

modulo.controller( "ListadoRolCtrl", require( "./controladores/listadoRolCtrl.js" ) );

modulo.controller( "FormRolCtrl", require( "./controladores/formRolCtrl.js" ) );

modulo.factory( "RolAPI", require( "./controladores/rest/rolAPI.js" ) );

module.exports = modulo;
