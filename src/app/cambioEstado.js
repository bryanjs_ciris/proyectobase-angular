"use strict";

module.exports = cambioEstado;
var validador = require( "./base/validarPermisos.js" );

function cambioEstado( $auth, $localStorage, $sessionStorage, Notificaciones, $state, $http ) {
  return function( event, toState ) {
    if ( $auth.isAuthenticated() ) {
      prevenirPantallaDeLogin( event, toState );
      validarPermisos( event, toState );
      headerOdoo();
      deslogueoEnOtroLugar( event );
    } else {
      if ( $localStorage.usuario ) {
        logueoPorRecuerdo( event );
      } else {
        sinLogin( event, toState );
      }
    }
  };

  //Está logueado y quiere ir a login
  function prevenirPantallaDeLogin( event, toState ) {
    if ( toState.name === "login" ) {
      inicio( event );
    }
  }

  function validarPermisos( event, toState ) {
    if ( !validador( toState.name, $auth.getPayload().permisos ) ) {
      event.preventDefault();
      Notificaciones.agregarPersonalizado( 403,
       "Usted no tiene los permisos requeridos para esta acción" );
      $state.go( "403" );
    }
  }

  //Está logueado y se setean los valores de cabeceras default
  function headerOdoo() {
    if ( $sessionStorage.usuario ) {
      $http.defaults.headers.common.odooSession = $sessionStorage.usuario.sesionOdoo;
    }
  }

  //Tiene credenciales en session pero no en local. Se desloguea entonces.
  function deslogueoEnOtroLugar( event ) {
    if ( !$localStorage.usuario && !$sessionStorage.usuario ) {
      event.preventDefault();
      $auth.logout();
      delete $sessionStorage.usuario;
      Notificaciones.agregarPersonalizado( 400, "Ha finalizado la sesión desde otro lugar" );
      $state.go( "login" );
    }
  }

  /*Tiene credenciales en localStorage pero no en session.
  Se carga entonces los credenciales en session*/
  function logueoPorRecuerdo( event ) {
    $sessionStorage.usuario = $localStorage.usuario;
    $auth.setToken( $sessionStorage.usuario.token );
    inicio( event );
    Notificaciones.agregarPersonalizado( 200, "Bienvenido de vuelta " +
     $sessionStorage.usuario.nombre );
  }

  //No está logueado del todo y quiere ver un recurso que necesita logueo
  function sinLogin( event, toState ) {
    if ( !toState.libre ) {
      event.preventDefault();
      Notificaciones.agregarPersonalizado( 400,
         "Necesita estar autenticado para ver estos recursos" );
      $state.go( "login" );
    }
  }

  function inicio( event ) {
    event.preventDefault();
    $state.go( "inicio.principal" );
  }
}
