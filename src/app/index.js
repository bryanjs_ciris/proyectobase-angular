/**
  Proyecto Base Ciris Informatic Solutions
  Enero 2015
  Recursos y lecturas
  Guia de estilo: https://github.com/johnpapa/angularjs-styleguide
  Otra guia de estilo: http://toddmotto.com/opinionated-angular-js-styleguide-for-teams/
  Sobre los controladores: http://toddmotto.com/rethinking-angular-js-controllers/
  Estilos Bootstrap: http://getbootstrap.com/css/
  Tema base de bootstrap: http://bootswatch.com/paper/
  notificaciones: https://github.com/Foxandxss/angular-toastr
*/
"use strict";
var nombre = require( "../../package.json" ).name;
var nombreNatural = require( "../../package.json" ).nombreNatural;
var prefix = "ciris-" + nombre;

moment.locale( "es" );

//URL del backend en constante "urlApi"
var modulo = angular.module( nombre, [
  "ngAnimate",
  "ngMessages",
  "ngTouch",
  "ngStorage",
  "ui.router",
  "ui.bootstrap",
  "toastr",
  "satellizer",
  "angular-loading-bar",
  "angularUtils.directives.uiBreadcrumbs",
  "Backend",
  require( "./base" ).name,
  require( "./login" ).name,
  require( "./principal" ).name,
  require( "./administracion" ).name
] );

modulo.config( config );
modulo.run( run );

config.$inject = [
  "$locationProvider",
  "$compileProvider",
  "toastrConfig",
  "$authProvider",
  "urlApi",
  "$localStorageProvider",
  "$sessionStorageProvider",
  "cfpLoadingBarProvider" ];

function config( $locationProvider, $compileProvider, toastrConfig, $authProvider, urlApi,
   $localStorageProvider, $sessionStorageProvider, cfpLoadingBarProvider ) {
  modoHtml5( true );
  $compileProvider.debugInfoEnabled( false );

  function modoHtml5( modo ) {
    if ( window.history && window.history.pushState ) {
      $locationProvider.html5Mode( {
        enabled: modo,
        requireBase: false
      } );
    }
  } //modoHtml5
  $localStorageProvider.setKeyPrefix( prefix + "-" );
  $sessionStorageProvider.setKeyPrefix( prefix + "-" );
  configurarToastr( toastrConfig );
  configurarSatellizer( $authProvider, urlApi );
  configurarLoadingBar( cfpLoadingBarProvider );

} //function

run.$inject = [ "$rootScope", "$auth", "$state", "Notificaciones",
 "$http", "$sessionStorage", "$localStorage" ];

function run( $rootScope, $auth, $state, Notificaciones,
   $http, $sessionStorage, $localStorage ) {
  var cambioEstadoPar = require( "./cambioEstado.js" );
  var cambioEstado = cambioEstadoPar( $auth, $localStorage,
     $sessionStorage, Notificaciones, $state, $http );
  $rootScope.$on( "$stateChangeStart", cambioEstado );
  $rootScope.$on( "$stateChangeError", noEncontrado );

  function noEncontrado( event ) {
    event.preventDefault();
    $state.go( "404" );
  }
}

function configurarSatellizer( $authProvider, url ) {
  $authProvider.storageType = "sessionStorage";
  $authProvider.loginUrl = url + "/api/login/odoo";
  $authProvider.baseUrl = url;
  $authProvider.tokenPrefix = prefix;
  $authProvider.google( {
    clientId: "738077328040-4opl3gua0qjr3a5jm015oa0rbid0lgoa.apps.googleusercontent.com",
    url: "/api/login/google"
  } );
  $authProvider.oauth1( {
    name: "odoo",
    url: "/api/login/odoo",
    redirectUri: window.location.origin || window.location.protocol +
     "//" + window.location.host + "/asfaltos"
  } );
}

function configurarToastr( toastrConfig ) {
  angular.extend( toastrConfig, {
    autoDismiss: true,
    containerId: "toast-container",
    maxOpened: 0,
    newestOnTop: true,
    positionClass: "toast-top-right",
    preventDuplicates: true,
    preventOpenDuplicates: true,
    target: "body",
    allowHtml: true,
    closeButton: true,
    closeHtml: "<i class='fa fa-fw fa-times'></i>",
    extendedTimeOut: 25000,
    iconClasses: {
      error: "alert-danger",
      info: "alert-info",
      success: "alert-success",
      warning: "alert-warning"
    },
    messageClass: "toast-message",
    onHidden: null,
    onShown: null,
    onTap: null,
    progressBar: true,
    tapToDismiss: true,
    timeOut: 10000,
    titleClass: "toast-title",
    toastClass: "alert"
  } );
}

function configurarLoadingBar( cfpLoadingBarProvider ) {
  cfpLoadingBarProvider.includeSpinner = false;
  cfpLoadingBarProvider.loadingBarTemplate = "<div id='loading-bar'>" +
    "<aside class='splash-title'>" +
      "<h1>" + nombreNatural + "</h1>" +
      "<div class='bar'></div>" +
      "<p>Estamos procesando su pedido, por favor espere.</p>" +
      "<img src='recursos/imagenes/loading-bars.svg' " +
      "width='64' height='64' class='animated fadeIn'/>" +
    "</aside>" +
  "</div>";
}

module.exports = modulo;

angular.element( document ).ready( function() {
  angular.bootstrap( document, [ modulo.name ], {strictDi: true} );
} );
